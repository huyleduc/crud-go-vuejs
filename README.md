# CRUD Go VueJs


This application demonstrates a simple CRUD (Create, Read, Update, Delete) example using Go for the backend and Vue.js for the frontend. It's a great starting point for understanding how to build and manage a full-stack application.

## Prerequisites

- Docker

## Configuration

Before starting the application, you need to copy the sample environment configuration file. You can do this by running:

```bash
cp .env_sample .env
```

This will set up the environment variables needed for the application to run. Feel free to edit the \`.env\` file if you need to customize any settings.

## Starting the Application

You can build and start the application using Docker Compose. Run the following command in the root directory:

```bash
docker-compose up --build
```

Once the application is started, the frontend can be accessed at `http://localhost:8091`.

## Further Details

For more specific information about the frontend and backend components, please refer to the respective README files located in the \`front\` and \`backend\` folders.

## Testing

coming soon

## Contributing

coming soon

## Support

For any questions, issues, or support, please contact .
coming soon

## License

coming soon




