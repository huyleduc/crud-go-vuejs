
# Go Backend - CRUD Example Application

The backend of this CRUD example application is developed in Go, utilizing several modern libraries and technologies.





## Migration

The migration in this project is managed using `go-migrate`. You can create a new migration by executing the command below:

```bash
docker run -u $(id -u ${USER}):$(id -g ${USER}) \
           -v $(pwd)/migrations:/migrations \
           migrate/migrate\
           create -ext sql -dir /migrations -seq <migration_name>
```

Replace `<migration_name>` with your specific migration name.

## Technologies and Dependencies

### Gin

Gin is a web framework used in this project for creating the API. It provides a robust set of features for building scalable and optimized web applications.

### Golang-Migrate

Golang-Migrate is utilized for database migration, allowing you to manage and automate changes to the database schema.

### GORM

GORM is an ORM library for Golang, simplifying the interaction with the database.

### Wire
Wire is a code generation tool provided by Google that automates connecting components using dependency injection. It analyzes the application's types and variables to generate code that initializes them without relying on reflection, leading to a more efficient and maintainable codebase.


### Other Dependencies

Here is the list of all dependencies used in the project:

```go
module backend
go 1.20

require (
    github.com/gin-gonic/gin v1.9.1
    github.com/golang-migrate/migrate/v4 v4.16.2
    github.com/google/wire v0.5.0
    gorm.io/driver/postgres v1.5.2
    gorm.io/gorm v1.25.2
    // Additional indirect dependencies
)
```

## Running the Application

Use main application READ.ME instructtion to deploy with `docker compose`


## Testing
Coming soon
```




