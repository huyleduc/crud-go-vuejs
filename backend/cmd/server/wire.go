//go:build wireinject
// +build wireinject
package main

import (
	"backend/internal/app/dao"
	"backend/internal/app/service"
	"backend/internal/app/handler"
	"backend/internal/app"
	"backend/internal/pkg/database"
	"backend/internal/app/util"
	"github.com/google/wire"
	"github.com/gin-gonic/gin"
)

func InitializeApp(config util.Config) (*gin.Engine, error) {
	wire.Build(
		database.ProvideDB,
		dao.ProviderSet,
		service.ProviderSet,
		handler.ProviderSet,
		app.Router,
	)

	return &gin.Engine{}, nil
}

