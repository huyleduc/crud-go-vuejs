package main

import (
	"backend/internal/app/util"
	"backend/internal/pkg/database"
	"log"
	"net/http"
)

func main() {
	config, err := util.LoadConfig()
	if err != nil {
		log.Fatalf("cannot load config: %v", err)
	}

    database.RunDBMigration(config.MigrationURL, config.DBSource)

	router, err := InitializeApp(config)
	if err != nil {
		log.Fatalf("Failed to initialize app: %v", err)
	}

	log.Fatal(http.ListenAndServe(":8080", router))
}

