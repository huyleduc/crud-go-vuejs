package database

import (
	"fmt"
    "log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
    "backend/internal/app/util"

  	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)


func ProvideDB(config util.Config) (*gorm.DB, error) {
	return Connect(config)
}


func Connect(config util.Config) (*gorm.DB, error) {
    dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable TimeZone=Europe/Berlin",
		config.DBHost, config.DBUser, config.DBPassword, config.DBName, config.DBPort)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		return nil, err
	}

	return db, nil
}

func RunDBMigration(migrationURL string, dbSource string) {


    if migrationURL == "" || dbSource == "" {
        log.Fatalf("MIGRATION_URL or POSTGRES_LINK not set")
    }

    migration, err := migrate.New(migrationURL, dbSource)
    if err != nil {
        log.Fatalf("cannot create new migrate instance: %v", err)
    }

    if err = migration.Up(); err != nil && err != migrate.ErrNoChange {
        log.Fatalf("failed to run migrate up: %v", err)
    }

    log.Println("db migrated successfully")
}
