package util

import (
	"os"
	"strconv"
    "fmt"
)

type Config struct {
	DBHost     string `mapstructure:"DB_HOST"`
	DBName     string `mapstructure:"DB_NAME"`
	DBPort     int    `mapstructure:"DB_PORT"`
	DBUser     string `mapstructure:"DB_USER"`
	DBPassword string `mapstructure:"DB_PASSWORD"`
    MigrationURL string `mapstructure:"MIGRATION_URL"`
    DBSource string
}

func LoadConfig() (Config, error) {
      port, err := strconv.Atoi(os.Getenv("DB_PORT"))
	  if err != nil {
	  	return Config{}, fmt.Errorf("could not convert DB_PORT to an integer: %v", err)
	  }
    	config := Config{
		DBHost:       os.Getenv("DB_HOST"),
		DBName:       os.Getenv("DB_NAME"),
		DBPort:       port,
		DBUser:       os.Getenv("DB_USER"),
		DBPassword:   os.Getenv("DB_PASSWORD"),
		MigrationURL: os.Getenv("MIGRATION_URL"),
	}
    	config.DBSource = fmt.Sprintf("postgresql://%s:%s@%s:%d/%s?sslmode=disable",
		config.DBUser, config.DBPassword, config.DBHost, config.DBPort, config.DBName)


	return config, nil
}

