package dao

import (
	"gorm.io/gorm"
	"backend/internal/app/model"
)

type AuthorRepository struct {
	DB *gorm.DB
}

func ProvideAuthorRepository(db *gorm.DB) *AuthorRepository {
	return &AuthorRepository{DB: db}
}

func (r *AuthorRepository) Create(author *model.Author) error {
	if err := r.DB.Create(author).Error; err != nil {
		return err
	}
	return nil
}

func (r *AuthorRepository) GetAll() ([]model.Author, error) {
    var authors []model.Author
    if err := r.DB.Find(&authors).Error; err != nil {
        return nil, err
    }
    return authors, nil
}

func (r *AuthorRepository) GetByID(id uint) (*model.Author, error) {
    var author model.Author
    result := r.DB.First(&author, id)
    if result.Error != nil {
        return nil, result.Error
    }
    return &author, nil
}

func (r *AuthorRepository) Update(author *model.Author) error {
    result := r.DB.Save(author)
    if result.Error != nil {
        return result.Error
    }
    return nil
}

func (r *AuthorRepository) Delete(id uint) error {
    var author model.Author
    if err := r.DB.First(&author, id).Error; err != nil {
        return err
    }

    if err := r.DB.Delete(&author).Error; err != nil {
        return err
    }

    return nil
}

