package handler

import (
	"net/http"
	"strconv"

	"backend/internal/app/model"
	"backend/internal/app/model/dto"
	"backend/internal/app/service"

	"github.com/gin-gonic/gin"
)

type AuthorHandler struct {
	Service *service.AuthorService
}

func ProvideAuthorHandler(service *service.AuthorService) *AuthorHandler {
	return &AuthorHandler{Service: service}
}

func (h *AuthorHandler) CreateAuthor(c *gin.Context) {
	var authorDto dto.AuthorDto
	if err := c.ShouldBindJSON(&authorDto); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	newAuthor := &model.Author{
		FirstName: authorDto.FirstName,
		LastName:  authorDto.LastName,
		BirthYear: authorDto.BirthYear,
	}

	err := h.Service.CreateAuthor(newAuthor)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create author"})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "Author created successfully", "data": newAuthor})
}

func (h *AuthorHandler) GetAllAuthors(c *gin.Context) {
    authors, err := h.Service.GetAllAuthors()
    if err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to retrieve authors"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": authors})
}


func (h *AuthorHandler) GetAuthorByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	author, err := h.Service.GetAuthorByID(uint(id))
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Author not found"})
		return
	}
	c.JSON(http.StatusOK, author)
}

func (h *AuthorHandler) UpdateAuthor(c *gin.Context) {
    id, _ := strconv.Atoi(c.Param("id"))
    authorDto := dto.AuthorDto{}
    if err := c.ShouldBindJSON(&authorDto); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
        return
    }

    updatedAuthor := &model.Author{
        ID:        uint(id),
        FirstName: authorDto.FirstName,
        LastName:  authorDto.LastName,
        BirthYear: authorDto.BirthYear,
    }

    err := h.Service.UpdateAuthor(updatedAuthor)
    if err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update author"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"message": "Author updated successfully", "data": updatedAuthor})
}

func (h *AuthorHandler) DeleteAuthor(c *gin.Context) {
    id, _ := strconv.Atoi(c.Param("id"))

    err := h.Service.DeleteAuthor(uint(id))
    if err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete author"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"message": "Author deleted successfully"})
}

