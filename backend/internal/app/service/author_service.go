package service

import (
    "fmt"
	"backend/internal/app/dao"
	"backend/internal/app/model"
)

type AuthorService struct {
	Repo *dao.AuthorRepository
}

func ProvideAuthorService(repo *dao.AuthorRepository) *AuthorService {
	return &AuthorService{Repo: repo}
}

func (s *AuthorService) CreateAuthor(author *model.Author) error {

	if err := s.Repo.Create(author); err != nil {
		return fmt.Errorf("failed to create author: %w", err)
	}

	return nil
}

func (s *AuthorService) GetAllAuthors() ([]model.Author, error) {
    authors, err := s.Repo.GetAll()
    if err != nil {
        return nil, err
    }
    return authors, nil
}


func (s *AuthorService) GetAuthorByID(id uint) (*model.Author, error) {
	return s.Repo.GetByID(id)
}

func (s *AuthorService) UpdateAuthor(author *model.Author) error {
    existingAuthor, err := s.Repo.GetByID(author.ID)
    if err != nil {
        return err
    }

    // Update the fields of the existing author with the new data
    existingAuthor.FirstName = author.FirstName
    existingAuthor.LastName = author.LastName
    existingAuthor.BirthYear = author.BirthYear

    return s.Repo.Update(existingAuthor)
}

func (s *AuthorService) DeleteAuthor(id uint) error {
    err := s.Repo.Delete(id)
    return err
}

