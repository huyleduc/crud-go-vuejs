package service

import "github.com/google/wire"

var ProviderSet = wire.NewSet(
	ProvideAuthorService,
	// Other providers in service package
)

