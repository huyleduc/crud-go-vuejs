package app

import (

    "backend/internal/app/handler"
	"github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

func HelloHandler(c *gin.Context) {
	c.String(200, "Hello, world!")
}

func Router(db *gorm.DB, authorHandler *handler.AuthorHandler) *gin.Engine {
	r := gin.Default()

    r.Use(func(c *gin.Context) {
    c.Set("db", db)
    c.Next()
	})


	api := r.Group("/api")
	{
		api.GET("/hello", HelloHandler)

        authors := api.Group("/authors")
		{
			authors.GET("", authorHandler.GetAllAuthors)
			authors.GET("/:id", authorHandler.GetAuthorByID)
            authors.POST("", authorHandler.CreateAuthor)
            authors.PUT("/:id", authorHandler.UpdateAuthor)
            authors.DELETE("/:id", authorHandler.DeleteAuthor)
		}
	}

	return r
}


