INSERT INTO authors (first_name, last_name, birth_year)
VALUES
    ('Søren', 'Kierkegaard', 1813),
    ('Haruki', 'Murakami', 1949),
    ('J.R.R.', 'Tolkien', 1892);

